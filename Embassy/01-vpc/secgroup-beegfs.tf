resource "openstack_compute_secgroup_v2" "BeeGFS" {
  name        = "${local.namespace}-BeeGFS"
  description = "Allow BeeGFS traffic"
  rule {
    ip_protocol = "tcp"
    from_port   = 8003
    to_port     = 8003
    cidr        = "0.0.0.0/0"
  }
  rule {
    ip_protocol = "udp"
    from_port   = 8003
    to_port     = 8003
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 8004
    to_port     = 8004
    cidr        = "0.0.0.0/0"
  }
  rule {
    ip_protocol = "udp"
    from_port   = 8004
    to_port     = 8004
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 8005
    to_port     = 8005
    cidr        = "0.0.0.0/0"
  }
  rule {
    ip_protocol = "udp"
    from_port   = 8005
    to_port     = 8005
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 8006
    to_port     = 8006
    cidr        = "0.0.0.0/0"
  }
  rule {
    ip_protocol = "udp"
    from_port   = 8006
    to_port     = 8006
    cidr        = "0.0.0.0/0"
  }

  rule {
    ip_protocol = "tcp"
    from_port   = 8008
    to_port     = 8008
    cidr        = "0.0.0.0/0"
  }
  rule {
    ip_protocol = "udp"
    from_port   = 8008
    to_port     = 8008
    cidr        = "0.0.0.0/0"
  }
}
