data "terraform_remote_state" "state" {
  backend = "local"
  config {
    path = "../00-config/terraform.tfstate"
  }
}

locals {
  namespace = "${data.terraform_remote_state.state.namespace}"
  subnet = "${data.terraform_remote_state.state.subnet}"
  external_network = "${data.terraform_remote_state.state.external_network}"

  state = "../${data.terraform_remote_state.state.state}"

  default_security_group = "${data.terraform_remote_state.state.default_security_group}"
  public_key = "${data.terraform_remote_state.state.public_key}"
  private_key = "${data.terraform_remote_state.state.private_key}"

  tenancy_user = "${data.terraform_remote_state.state.tenancy_user}"
}
