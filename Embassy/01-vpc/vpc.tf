data "openstack_networking_network_v2" "extnet" {
  name = "${local.external_network}"
}

resource "openstack_networking_floatingip_v2" "floatingip" {
  pool = "${local.external_network}"
}

resource "openstack_networking_network_v2" "network" {
  name           = "${local.namespace}-network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = "${local.namespace}-subnet"
  network_id      = "${openstack_networking_network_v2.network.id}"
  cidr            = "${local.subnet}"
  ip_version      = 4
  dns_nameservers = ["8.8.8.8", "8.8.4.4"]                          # Google public DNS servers
}

resource "openstack_networking_router_v2" "router" {
  depends_on = [
    # not sure why, but this seems to be necessary...
    "openstack_networking_network_v2.network",
  ]

  name                = "${local.namespace}-router"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.extnet.id}"
  # enable_snat         = "true" # This is not allowed on Embassy
}

resource "openstack_networking_router_interface_v2" "interface" {
  router_id = "${openstack_networking_router_v2.router.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet.id}"
}

resource "null_resource" "vpc-ssh-config" {

  depends_on = [
    "openstack_networking_floatingip_v2.floatingip",
    "openstack_compute_keypair_v2.keypair"
  ]

  triggers = { 
    something = "${uuid()}"
  }

  provisioner "local-exec" {
    command = "../bin/initialise-ssh-config.sh ${local.state} ${local.tenancy_user} ${openstack_networking_floatingip_v2.floatingip.address} ${local.private_key}"
  }

  provisioner "local-exec" {
    command = <<EOF
[ -f ${local.private_key} ] && /bin/rm -f ${local.private_key}
echo "${openstack_compute_keypair_v2.keypair.public_key}"  | tee ${local.public_key}
echo "${openstack_compute_keypair_v2.keypair.private_key}" | tee ${local.private_key}
chmod 400 ${local.private_key}
EOF
  }

}