output "auth_url" {
  value = "${var.auth_url}"
}

output "availability_zone" {
  value = "${var.availability_zone}"
}

output "bastion_disk_gb" {
  value = "${var.bastion_disk_gb}"
}

output "bastion_flavour" {
  value = "${var.bastion_flavour}"
}

output "bastion_image" {
  value = "${var.bastion_image}"
}

output "bastion_volumes" {
  value = "${var.bastion_volumes}"
}

output "domain" {
  value = "${var.domain}"
}

output "external_network" {
  value = "${var.external_network}"
}

output "host_count" {
  value = "${var.host_count}"
}

output "groups" {
  value = "${var.groups}"
}

output "host_disk_gb" {
  value = "${var.host_disk_gb}"
}

output "host_flavour" {
  value = "${var.host_flavour}"
}

output "host_image" {
  value = "${var.host_image}"
}

output "lsf_host_range" {
  value = "${var.lsf_host_range}"
}

output "lsf_version" {
  value = "${var.lsf_version}"
}

output "lsf_source" {
  value = "${var.lsf_source}"
}

output "default_security_group" {
  value = "${var.default_security_group}"
}

output "globus_connect" {
  value = "${var.globus_connect}"
}

output "namespace" {
  value = "${var.namespace}"
}

output "password" {
  sensitive = true
  value = "${var.password}"
}

output "private_key" {
  value = "../${var.state}/${var.private_key}"
}

output "public_key" {
  value = "../${var.state}/${var.public_key}"
}

output "subnet" {
  value = "${var.subnet}"
}

output "tenancy" {
  value = "${var.tenancy}"
}

output "tenancy_user" {
  value = "${var.tenancy_user}"
}

output "cloud_user" {
  value = "${var.cloud_user}"
}

output "state" {
  value = "${var.state}"
}

output "users" {
  value = "${var.users}"
}
