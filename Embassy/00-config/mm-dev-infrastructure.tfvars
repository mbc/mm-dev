namespace = "mm-dev"

external_network = "ext-net"
tenancy_user = "centos"

bastion_image = "centos7"
bastion_disk_gb = 60
bastion_flavour = "s1.large"

lsf_version = 10.1
lsf_source = "../LSF" # Relative to the directory with the lsf terraform installation script

bastion_volumes = [
  {
    mount = "/lsf",
    size = 50,
    label = "LSF"
  },
  {
    mount = "/home/user",
    size = 100,
    label = "USER",
    export = "rw"
  },
  {
    mount = "/data",
    size = 750,
    label = "DATA",
    export = "rw",
    mode = "3777",
    group = "mm"
  },
]

host_image = "centos7"
host_disk_gb = 60
host_flavour = "s1.gargantuan"
host_count = 20

groups = [
  { name = "mm" } #, id = "" }
]

users = [
  {
    user = "lsfadmin",
    id   = "1001",
    sudo = "yes",
    home = "/home/user/lsfadmin",
  },
  {
    user = "lsfuser",
    id   = "1002",
    sudo = "no",
    home = "/home/user/lsfuser",
  },
  {
    user = "maxim",
    id   = "2877",
    sudo = "no",
    home = "/home/user/maxim",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9qMALafVAJc5m3+lA3Qva0JdmgzZI+JXaJkeCwZSnQ+VIybkGFehSIGYsF4pggfMQcRVaNhfCoo5xa8oEubcu6rhUMaeT4CxK1sdXLlZSjV7aAJ2Zc6NMPWZzUcbXId+dfLPtmEo+SaB6dbcQHlGWg1Tkx2Sy9a5mZKsWoQTAw10kQW4lsv91xF+5dTP7QVxVWtb1tpjbmO696mRlnrrH/5VRtAQK/XQQjrDLJRvefgRDqyT0RIZdZYT5GBbt950owGOWdyU3XMVnvUvZWkedepsBMXyq/M7P4x1+/dvdlkfASQHA/hbOrJYiLWvi5S5/9xCLluFuS/grYvvQ50XgRoDihS86bVAtmFHj1c8GVhYZz40AVVSHlg7ifzMZ9er6IHfIaqQjZ4meJU8RNkZ9tyFMeovIovzNHwwbN6RGhpwJgdHkOEy4nSV7HkGAIrQ4XtjGNIpHPB71SQzGoiFMRXtyBAHqE+vFHLyChWqvNqu3bLPea2Sa9yOaJ8pjltt9Hyff4mTUhJ3kDkVwoNNhfYHMyHQAqYQgdMXHChhRQiOLY0aybu2Y0dFR07OWfutIP5weMy3jSs2hSzDTDz+SxtMiLoraazZj3jomumnnoVvCuLuOVaJMduw+I+Xzj9bh/+zAdcBZT7FyeHNhyAqCAmd6maMCGgJ/c5jvZBP+UQ== maxim@ebi.ac.uk"
  },
  {
    user = "mdb",
    id   = "7149",
    sudo = "no",
    home = "/home/user/mdb",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLH7gWqWZZidUwa0WF/muGMU2K8dL8vwRA814tD+sdsNvJavu7baVTmYkqOuTt47rylsIJnDc2ZFJCyl9z07CMRNhnvp10CDpm7WPxW/iJJUGMYscA9QfyJDdHQo9kxM6OsnXFoZ36VgHoZu+p1v9OWNKPsz+tuw7KMyzLUguC/qpjQQGTlPzoeRIqkd7iwAfKqKX0CsvPlidfSSG3n9edgqmDhD0LBawxNFRGFxdOwX/Ydlgukzp88/1P5UVZNo7GmLZE+tIpbsmqdGLJayLeYC8ve15ohiXZVjy9cVqB75Kvr/p3AElujyEZxe3NFyogvuWof07ljoEWpGjbvrZ5/5xi5y1J6KQrwy7R/h71tyxFlnL//gZ0rQ2ywriVYbg0Mtf887qBJ2K7SiEajqTkFm49cuRYj1wnGEmCFbYb1GjDICME4RSaOOEf6N858KIPW04R4eYYQ7OeQVGet+yyhv/KVX6eFiwEywB7cwRXTNY9eQcjApmasm4iT6WvM/XPgU0AfLD2EyUlTFNxR9UuJU+TG6US5LiU6SmCExaYMyZRaABhevm/dTBRZMSsOwjHMCnb85tsUptpUX/oYIxDtNo9J5efOCAp7xKCsj3kMKc7h/+8CaEMXpw1JJOk/fGdrv94gN3SYrJd6XkHlbMwrzDcoGq0UUBkqFrZtga7Tw== mdb@ebi.ac.uk"
  },
  {
    user = "kates",
    id   = "8592",
    sudo = "no",
    home = "/home/user/kates",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwo5UESGqxL3LDjnYzAw4/mD0MiuGvK21TPzK8nYepV9kr5ZfIu5N47xHIgPx3T3YXljiT8+lUKZKWIVIdepp5fYhFyOBXTeD3DIkjlcbZU7hpdm31z7jSL3qATtcN1Uvkpj4Ppsw9rarQe9RzDYIEY3j6yYUFpTznRxpJ++jbFu51HizL1VsKbIKiN2LRCCFH3iI4lGaX7ZZA06jP03T05ulqFfxP8oZQdWaOXVPztBoGdojXFv9oEzXJtsvNxrhd9R+U8UR3T4WUiPr9/CSqM/3PkZHowM3YDRRMM/i5bk0XcZkYAbjid7yji3objiXofG1+KV3fvr0kh8p0mx+B kates@C02TV020HV2T"
  },
  {
    user = "cdtiwari",
    id   = "8110",
    sudo = "yes",
    home = "/home/user/cdtiwari",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+UiGtQdbAjRagQGebeRBcjit51tw/fx3nz4TnCbeEVdJI6k2710tiU67HXOaX6aFN3XyPKlt+IUgVWLDjA2Qv5tQdZpLvEsX4uE5ZQGqXxOnOaxaO8mav4SjYy+vRAgFJzmnKxPA8lS+iREEgIIDQDjjV9TMmZu9YQ9bZX2lU+4K3nCWf//aenQ73yW+ZgvxYWyO9rMwvX/3zmlaLBMNa5bLd3mqR02gg+t+DJ673S2nSGV4lVLJjocwWUvEzBhlyIPaIBU82hN7oHURxQXHrISooaGkdEd3tv4HvvrhpVEXquvP0IxsP6rAmi3OpgbaO0f9wLBskJe3v5rs6Je7Y2H8P1XuDI1X1wU+dqRkmNKaOHjt/Hk/mZStFwxo60JJHEQuzvH3xmr3ZQlvrKBL7TRJMcLepda4CmDkZXUbr5wLwDZA912noGWp4Z32bXW7k8eocAbAROrgVls89cke5bHQ5cD/yMqln05uGYnLszcUGN4R8T4LXWJC2eeOn2V22kLumArQwNspxr0FGN/i24pzx/Pjd564mXhQWeeRYXAWGaiXIaBKL2Q14mSpJ5tVQNufHHnGctK/reLaajyYcbbn1agqaOABBJJDVeUUE8M1jpJN/q+Fchu8/JG7ADfJXrZyKJc0YiQm8e76E125/pdlK7PHeioWcKj+7gswlDw== cdtiwari@ebi.ac.uk"
  },
  {
    user = "gcuser",
    groups = [ "mm" ],
    sudo = "no"
  }
]

globus_connect = {
  setup_key = "4908715f-9162-4274-8cab-35ee77d9a2bb",
  restrict_paths = "rw/data,r/output",
  user = "gcuser"
}
