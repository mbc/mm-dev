namespace = "spades"

# availability_zone = "AZ_2:overcloud-compute-63.localdomain"

external_network = "ext-net-37"
tenancy_user = "centos"

bastion_image = "centos7"
bastion_disk_gb = 20
bastion_flavour ="c1.mm-dev-spades-high-mem-1"

lsf_version = 10.1
lsf_source = "../LSF" # Relative to the directory with the lsf terraform installation script

bastion_volumes = [
  {
    mount = "/lsf",
    size = 50,
    label = "LSF"
  },
  {
    mount = "/home/user",
    size = 100,
    label = "USER",
    export = "rw"
  },
  {
    mount = "/data",
    size = 750,
    label = "DATA",
    export = "rw",
    mode = "3777",
    group = "mm"
  },
]

host_image = "centos7"
host_disk_gb = 20
host_flavour = "c1.mm-dev-spades-high-mem"
host_count = 4

groups = [
  { name = "mm" } #, id = "" }
]

users = [
  {
    user = "lsfadmin",
    id   = "1001",
    sudo = "yes",
    home = "/home/user/lsfadmin",
  },
  {
    user = "lsfuser",
    id   = "1002",
    sudo = "no",
    home = "/home/user/lsfuser",
  },
  {
    user = "maxim",
    id   = "2877",
    sudo = "no",
    home = "/home/user/maxim",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9qMALafVAJc5m3+lA3Qva0JdmgzZI+JXaJkeCwZSnQ+VIybkGFehSIGYsF4pggfMQcRVaNhfCoo5xa8oEubcu6rhUMaeT4CxK1sdXLlZSjV7aAJ2Zc6NMPWZzUcbXId+dfLPtmEo+SaB6dbcQHlGWg1Tkx2Sy9a5mZKsWoQTAw10kQW4lsv91xF+5dTP7QVxVWtb1tpjbmO696mRlnrrH/5VRtAQK/XQQjrDLJRvefgRDqyT0RIZdZYT5GBbt950owGOWdyU3XMVnvUvZWkedepsBMXyq/M7P4x1+/dvdlkfASQHA/hbOrJYiLWvi5S5/9xCLluFuS/grYvvQ50XgRoDihS86bVAtmFHj1c8GVhYZz40AVVSHlg7ifzMZ9er6IHfIaqQjZ4meJU8RNkZ9tyFMeovIovzNHwwbN6RGhpwJgdHkOEy4nSV7HkGAIrQ4XtjGNIpHPB71SQzGoiFMRXtyBAHqE+vFHLyChWqvNqu3bLPea2Sa9yOaJ8pjltt9Hyff4mTUhJ3kDkVwoNNhfYHMyHQAqYQgdMXHChhRQiOLY0aybu2Y0dFR07OWfutIP5weMy3jSs2hSzDTDz+SxtMiLoraazZj3jomumnnoVvCuLuOVaJMduw+I+Xzj9bh/+zAdcBZT7FyeHNhyAqCAmd6maMCGgJ/c5jvZBP+UQ== maxim@ebi.ac.uk"
  },
  {
    user = "mdb",
    id   = "7149",
    sudo = "no",
    home = "/home/user/mdb",
    groups = [ "mm" ],
    key    = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLH7gWqWZZidUwa0WF/muGMU2K8dL8vwRA814tD+sdsNvJavu7baVTmYkqOuTt47rylsIJnDc2ZFJCyl9z07CMRNhnvp10CDpm7WPxW/iJJUGMYscA9QfyJDdHQo9kxM6OsnXFoZ36VgHoZu+p1v9OWNKPsz+tuw7KMyzLUguC/qpjQQGTlPzoeRIqkd7iwAfKqKX0CsvPlidfSSG3n9edgqmDhD0LBawxNFRGFxdOwX/Ydlgukzp88/1P5UVZNo7GmLZE+tIpbsmqdGLJayLeYC8ve15ohiXZVjy9cVqB75Kvr/p3AElujyEZxe3NFyogvuWof07ljoEWpGjbvrZ5/5xi5y1J6KQrwy7R/h71tyxFlnL//gZ0rQ2ywriVYbg0Mtf887qBJ2K7SiEajqTkFm49cuRYj1wnGEmCFbYb1GjDICME4RSaOOEf6N858KIPW04R4eYYQ7OeQVGet+yyhv/KVX6eFiwEywB7cwRXTNY9eQcjApmasm4iT6WvM/XPgU0AfLD2EyUlTFNxR9UuJU+TG6US5LiU6SmCExaYMyZRaABhevm/dTBRZMSsOwjHMCnb85tsUptpUX/oYIxDtNo9J5efOCAp7xKCsj3kMKc7h/+8CaEMXpw1JJOk/fGdrv94gN3SYrJd6XkHlbMwrzDcoGq0UUBkqFrZtga7Tw== mdb@ebi.ac.uk"
  },
]
