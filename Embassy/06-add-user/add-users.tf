resource "null_resource" "adduser_ssh_keygen" {

  # triggers { something = "${uuid()}" }

  provisioner "local-exec" {
    command = <<EOF
echo '${jsonencode( local.users )}' | tee users.json >/dev/null
../bin/user-keygen.sh ${local.state} users.json
/bin/rm -f users.json
EOF
  }
}

resource "null_resource" "adduser_bastion" {

  depends_on = [
    "null_resource.adduser_ssh_keygen"
  ]

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  # triggers { something = "${uuid()}" }

  provisioner "file" {
    content     = "${jsonencode( local.groups )}"
    destination = "/home/${local.user}/groups.json"
  }
  provisioner "file" {
    content     = "${jsonencode( local.users )}"
    destination = "/home/${local.user}/users.json"
  }
  provisioner "file" {
    source = "${local.state}/keys"
    destination = "/home/${local.user}/.ssh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/add-groups.sh /home/${local.user}/groups.json",
      "sudo /home/${local.user}/bin/add-users.sh /home/${local.user}/users.json",
      "sudo /home/${local.user}/bin/add-groups-set-perms.sh /home/${local.user}/volume.metadata.json",
    ]
  }
}

resource "null_resource" "adduser_hosts" {

  depends_on = [
    "null_resource.adduser_bastion"
  ]

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
    host                = "${element(local.host_ips, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  count = "${local.host_count}"

  # triggers { something = "${uuid()}" }

  provisioner "file" {
    content     = "${jsonencode( local.users )}"
    destination = "/home/${local.user}/users.json"
  }
  provisioner "file" {
    source = "${local.state}/keys"
    destination = "/home/${local.user}/.ssh"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/add-users.sh /home/${local.user}/users.json",
    ]
  }
}
