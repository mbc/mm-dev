# data "template_cloudinit_config" "example" {
#   gzip          = true
#   base64_encode = true

#   # This would be great if it worked, but it doesn't :-(
#   part {
#     content_type = "text/cloud-config"
#     content      = "${file("cc.yaml")}"
#   }

#   # part {
#   #   content_type = "text/x-shellscript"
#   #   content      = "${file("${path.module}/scripts/bootstrap.sh")}"
#   # }

# }

data "openstack_images_image_v2" "bastion_image" {
  name        = "${local.bastion_image}"
  most_recent = true
}

locals {
  bastion_image_id = "${data.openstack_images_image_v2.bastion_image.id}"
}

resource "openstack_compute_instance_v2" "bastion" {

  availability_zone = "AZ_2:overcloud-compute-66.localdomain" # "${local.availability_zone}"
  flavor_name       = "${local.bastion_flavour}"
  security_groups   = [ "${local.security_groups}" ]
  name              = "${local.namespace}-bastion"

  block_device {
    uuid                  = "${local.bastion_image_id}"
    boot_index            = 0
    delete_on_termination = true
    destination_type      = "volume"
    source_type           = "image"
    volume_size           = "${local.bastion_disk_gb}"
  }

  # Optional ephemeral disk?
  # block_device {
  #   boot_index            = -1
  #   delete_on_termination = true
  #   destination_type      = "local"
  #   source_type           = "blank"
  #   guest_format          = "ext4"
  #   volume_size           = 100
  # }

  network {
    name = "${local.network}"
  }

  key_pair = "${local.key_pair}"

  # This is useless until the partitioning is taken into account properly
  # user_data = "${data.template_cloudinit_config.example.rendered}"
}

resource "openstack_compute_floatingip_associate_v2" "floatingip_bastion" {
  floating_ip           = "${local.floating_ip}"
  instance_id           = "${openstack_compute_instance_v2.bastion.id}"
  fixed_ip              = "${openstack_compute_instance_v2.bastion.access_ip_v4}"
  wait_until_associated = "true"
}

resource "openstack_compute_volume_attach_v2" "volume_attach" {
  count = "${length( local.bastion_volumes )}"
  instance_id = "${openstack_compute_instance_v2.bastion.id}"
  volume_id   = "${element( local.bastion_volume_ids, count.index )}"
}

resource "null_resource" "bastion_post_system_config" {

  depends_on = [
    "openstack_compute_floatingip_associate_v2.floatingip_bastion"
  ]
  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  provisioner "file" {
    source      = "../bin"
    destination = "/home/${local.user}"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 755 /home/${local.user}/bin/*",
      "sudo /home/${local.user}/bin/yum-packages.sh",
      "sudo /home/${local.user}/bin/fix-hostname.sh ${local.namespace}",
      "sudo /home/${local.user}/bin/firewalld.sh",
      "sudo /home/${local.user}/bin/nfs-server.sh",
    ]
  }

  provisioner "local-exec" {
    command =  "../bin/reboot-and-wait.sh bastion"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo yum update -y",
    ]
  }

}

resource "null_resource" "volume_mount" {

  depends_on = [
    "null_resource.bastion_post_system_config",
    "openstack_compute_volume_attach_v2.volume_attach"
  ]

  # triggers = { something = "${uuid()}" }

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  provisioner "file" {
    content = "${jsonencode( local.bastion_volumes )}"
    destination = "/home/${local.user}/volume.metadata.json"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/format-mount-export.sh /home/${local.user}/volume.metadata.json",
    ]
  }
}

resource "null_resource" "bastion_post_user_config" {

  depends_on = [
    "null_resource.bastion_post_system_config"
  ]

  # triggers = { something = "${uuid()}" }

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  provisioner "file" {
    source      = "${local.private_key}"
    destination = "/home/${local.user}/.ssh/ssh-key"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/sshd-fix.sh",
      "/home/${local.user}/bin/ssh-host-config.sh",
    ]
  }

  provisioner "local-exec" {
    command =  "../bin/ip-fragment.sh ${local.state} ${openstack_compute_instance_v2.bastion.access_ip_v4} bastion"
  }
}
