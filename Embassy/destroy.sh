#!/bin/bash

cloud=$1
if [ "$cloud" == "" ]; then
  n_conf=`ls 00-config/*-acct.tfvars 2>/dev/null | wc -l`
  if [ $n_conf == 1 ]; then
    cloud=`ls 00-config/*-acct.tfvars \
      | sed \
        -e 's%^.*/%%' \
        -e 's%-acct.tfvars$%%'`
    echo "Found config for '$cloud', using that..."
  else
    echo "Cannot uniquely identify your configuration"
    exit 0
  fi
fi

[ -d log ] || mkdir -p log

set -e
for dir in `ls | egrep '^[0-9]+-' | sort -r`
do
  echo " ==> $dir"
  cd $dir
  if [ -f ${cloud}-acct.tfvars ]; then
    vars="-var-file ${cloud}-acct.tfvars"
  fi
  if [ -f ${cloud}-infrastructure.tfvars ]; then
    vars="$vars -var-file ${cloud}-infrastructure.tfvars"
  fi
  terraform destroy --parallelism 50 \
    $vars \
    --auto-approve -no-color 2>&1 | tee ../log/destroy.$dir.log
  cd ..
done

rm -rf state/{fragments,hosts.dnsmasq,key,key.pub,ssh-*} log/apply*
