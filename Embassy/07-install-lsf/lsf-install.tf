resource "null_resource" "install_lsf_bastion" {

#  depends_on = [
#    "null_resource.bastion_dnsmasq",
#  ]

  connection {
    user        = "${local.user}"
    private_key = "${file(local.private_key)}"
    agent       = false
    host        = "${local.floating_ip}"
  }

  provisioner "file" {
    source      = "${local.lsf_source}"
    destination = "/home/${local.user}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/lsf-install.sh ${local.lsf_version} ${local.namespace} ${local.bastion_ip} ${local.lsf_host_range}",
      "sudo /home/${local.user}/bin/lsf-patch.sh ${local.lsf_version} /home/${local.user}/LSF/lsf10-patches.tgz",
      "sudo /home/${local.user}/bin/lsf-docker.sh ${local.lsf_version} ${local.namespace}",
    ]
  }
}

resource "null_resource" "install_lsf_hosts" {

  depends_on = [
    "null_resource.install_lsf_bastion",
    # "openstack_compute_instance_v2.host",
  ]

  count = "${local.host_count}"

  connection {
    user                = "${local.user}"
    private_key         = "${file(local.private_key)}"
    agent               = false
    host                = "${element(local.host_ips, count.index)}"
    bastion_private_key = "${file(local.private_key)}"
    bastion_host        = "${local.floating_ip}"
    bastion_user        = "${local.user}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo /home/${local.user}/bin/lsf-install.sh ${local.lsf_version} ${local.namespace}",
      "sudo /home/${local.user}/bin/lsf-docker.sh ${local.lsf_version} ${local.namespace}",
    ]
  }
}
