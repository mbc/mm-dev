#!/bin/bash

set -ex

chmod 600 ~/.ssh/ssh-key

curl http://169.254.169.254/2009-04-04/meta-data/public-keys/0/openssh-key | \
  tee ~/.ssh/ssh-key.pub
chmod 644 ~/.ssh/ssh-key.pub

user=`id --user --name`
config=~/.ssh/config
(
  echo "Host *"
  echo "  IdentityFile ~/.ssh/ssh-key"
  echo "  UserKnownHostsFile /dev/null"
  echo "  StrictHostKeyChecking no"
  echo "  user $user"
) | tee $config
chmod 600 $config
