#!/bin/bash

systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-lock
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-lock
systemctl start nfs-idmap

systemctl enable nfs-server
systemctl restart nfs-server

if [ "`which firewall-cmd`" !=  "" ]; then
  firewall-cmd --permanent --zone=public --add-service=nfs
  firewall-cmd --permanent --zone=public --add-service=mountd
  firewall-cmd --permanent --zone=public --add-service=rpc-bind
  firewall-cmd --reload
fi
