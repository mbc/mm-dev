#!/bin/bash

if [ "`which firewall-cmd`" !=  "" ]; then
  systemctl enable  dbus
  systemctl restart dbus
  systemctl restart systemd-logind # https://serverfault.com/questions/792486/ssh-connection-takes-forever-to-initiate-stuck-at-pledge-network
  systemctl enable  firewalld
  systemctl restart firewalld
fi
