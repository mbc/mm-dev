#!/bin/bash
(
  set -ex
  file=$1
  if [ "$file" == "" ]; then
    echo "Need a json file with mount point information"
    exit 1
  fi

  length=`cat $file | jq length`
  if [ "$length" == "" ]; then
    echo "No mount-point information, exiting..."
    exit 0
  fi

  index=0
  while [ $index -lt $length ]
  do
    export=`cat $file | jq .[${index}].export | tr -d '"'`
    label=` cat $file | jq .[${index}].label  | tr -d '"'`
    mode=`  cat $file | jq .[${index}].mode   | tr -d '"'`
    mount=` cat $file | jq .[${index}].mount  | tr -d '"'`
    name=`  cat $file | jq .[${index}].name   | tr -d '"'`
    size=`  cat $file | jq .[${index}].size   | tr -d '"'`

    index=`expr 1 + $index`

    if [ `lsblk | egrep "$mount$" | wc -l` -gt 0 ]; then
      echo "A volume is already mounted on $mount"
      continue
    fi

    [ -d $mount ] || mkdir -p $mount

    if [ "$export" == "rw" ]; then
      options="defaults,rw"
    else
      options="defaults,ro"
    fi

    cat /etc/fstab | egrep -v " $mount " | tee /etc/fstab.new >/dev/null
    mv /etc/fstab{.new,}
    echo "LABEL=$label $mount ext4 defaults 0 2" | tee -a /etc/fstab

    if [ "$export" == "rw" ]; then
      options='"*"(rw,sync,no_root_squash,no_all_squash)'
    else
      options='"*"(ro,sync,no_root_squash,no_all_squash)'
    fi

    [ -f /etc/exports ] || /bin/cp /dev/null /etc/exports
    cat /etc/exports | egrep -v "^$mount" | tee /etc/exports.new >/dev/null
    mv /etc/exports{.new,}
    echo "$mount $options" | tee -a /etc/exports

    #
    # See https://github.com/terraform-providers/terraform-provider-openstack/issues/277
    # OpenStack returns from attaching volumes before they're ready, which means we
    # have to poll for the device here.
    attempts=10
    bytes=`expr 1024 \* 1024 \* 1024 \* ${size}`
    while [ $attempts != "0" ]; do
      device=`lsblk -b | grep disk | egrep -v / | grep " $bytes " | cut -f 1 -d ' ' | head -1`
      if [ "$device" != "" ]; then
        break
      fi
      echo "No device found, waiting..., $attempts retries left"
      sleep 11
      attempts=`expr $attempts - 1`
    done

    if [ "$device" == "" ]; then
      echo "No device found after several attempts, spitting the dummy..."
      exit 1
    fi
    device="/dev/$device"

    echo "Found $device"

    options="-E lazy_itable_init=1,lazy_journal_init=1"
    if [ $size -gt 1000 ]; then
      options="$options -m 0 -O sparse_super,large_file"
    fi
    mkfs -t ext4 -L $label $options $device
    mount $mount

    if [ "$mode" != "-" ]; then
      chmod $mode $mount
    fi
  done

  service nfs-server restart
  echo "All done..."
) 2>&1 | tee /tmp/format-mount-export.log
