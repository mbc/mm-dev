#!/bin/bash

mkdir /data/test/

(
  echo "#!/bin/sh"
  echo "#BSUB -o /data/test/test.%J.out"
  echo "#BSUB -e /data/test/test.%J.err"
  echo " "
  echo "date"
  echo "hostname"
  echo "sleep 20"
  echo "echo 'All done'"
) | tee test.sh
chmod +x test.sh

for i in {0..1000}
do
  bsub < test.sh
done

bjobs
