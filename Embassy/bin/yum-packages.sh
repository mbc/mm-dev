#!/bin/sh

yum -y install epel-release.noarch
yum group install -y 'Development Tools'
yum install -y nfs-utils bind-utils mailx ansible git wget python-pip python-devel anaconda python34 \
    jq pbzip2 lbzip2 java
