#!/bin/bash

set -ex

ip=$1
namespace=$2
if [ "$namespace" == "" ]; then
  echo "Need the bastion IP and the namespace"
  exit 1
fi

echo "${ip} bastion ${namespace}-bastion" | tee -a /etc/hosts
