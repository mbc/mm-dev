#!/bin/bash

ephemerals=`blkid | \
  grep EPHEMERAL | \
  awk '{ print $2" "$1 }' | \
  sort | \
  awk '{ print $1 }' | \
  awk -F\" '{ print $2 }'`
echo "Found these ephemeral volumes: $ephemerals"

for ephemeral in $ephemerals
do
  device=`blkid | grep $ephemeral | awk '{ print $1 }' | tr -d ':'`
  echo "$ephemeral corresponds to $device"
  fstab=/etc/fstab

  current_mountpoint=`mount | grep "$device " | awk '{ print $3 }'`
  if [ "$current_mountpoint" != "" ]; then
    echo "Device is mounted on $current_mountpoint"
    mv $fstab{,.new}
    cat $fstab.new | egrep -v " $current_mountpoint " | tee $fstab >/dev/null
    /bin/rm -f $fstab.new
    umount $device
  else
    echo "Device is not currently mounted"
  fi
  mountpoint="/$ephemeral"
  mkdir -p $mountpoint

  echo "LABEL=$ephemeral $mountpoint auto defaults 0 2" | tee -a $fstab

  mkfs.xfs -f -L $ephemeral $device
  mount $mountpoint
  df -h $mountpoint
done
