#!/bin/bash

state_dir="`pwd`/state"
[ -d $state_dir ] || mkdir -p $state_dir

cloud=$1
if [ "$cloud" == "" ]; then
  n_conf=`ls 00-config/*-acct.tfvars 2>/dev/null | wc -l`
  if [ $n_conf == 1 ]; then
    cloud=`ls 00-config/*-acct.tfvars \
      | sed \
        -e 's%^.*/%%' \
        -e 's%-acct.tfvars$%%'`
    echo "Found config for '$cloud', using that..."
  else
    echo "Cannot uniquely identify your configuration"
    exit 0
  fi
fi

[ -d log ] || mkdir -p log
rm -f log/destroy*

for dir in `ls | egrep '^[0-9]+-'`
do
  echo " ==> $dir"
  vars=""
  cd $dir
  [ -d .terraform ] || terraform init

  if [ -f ${cloud}-acct.tfvars ]; then
    vars="-var-file ${cloud}-acct.tfvars"
  fi
  if [ -f ${cloud}-infrastructure.tfvars ]; then
    vars="$vars -var-file ${cloud}-infrastructure.tfvars"
  fi
  echo terraform apply --parallelism 50 $vars --auto-approve -no-color
  (
    terraform apply --parallelism 50 \
      $vars \
      --auto-approve -no-color 2>&1
      status=$?
    echo "Exit code from Terraform: $status"
    exit $status
  )

  status=$?
  if [ $status -ne 0 ]; then
    echo "Spitting the dummy..."
    exit $status
  fi

  cd ..
done
